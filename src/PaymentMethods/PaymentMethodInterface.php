<?php


namespace App\PaymentMethods;

interface PaymentMethodInterface
{
    public function getMethodName(): string;
}