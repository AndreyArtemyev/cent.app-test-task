<?php


namespace App\PaymentMethods;

use DateTime;

class Card implements PaymentMethodInterface
{
    const METHOD_NAME = 'Card';
    private string $pan;
    private DateTime $expiryDate;
    private int $cvc;

    public function __construct(string $pan, DateTime $expiryDate, int $cvc)
    {
        $this->pan = $pan;
        $this->expiryDate = $expiryDate;
        $this->cvc = $cvc;
    }

    public function getPan(): string
    {
        return $this->pan;
    }

    public function getExpiryDate(): DateTime
    {
        return $this->expiryDate;
    }

    public function getMethodName(): string
    {
        return self::METHOD_NAME;
    }
}