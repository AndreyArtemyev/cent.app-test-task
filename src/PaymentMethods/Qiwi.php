<?php


namespace App\PaymentMethods;

use DateTime;

class Qiwi implements PaymentMethodInterface
{
    const METHOD_NAME = 'Qiwi';
    private string $phone;

    public function __construct(string $phone)
    {
        $this->phone = $phone;
    }

    public function getPhone(): string
    {
        return $this->phone;
    }

    public function getMethodName(): string
    {
        return self::METHOD_NAME;
    }
}