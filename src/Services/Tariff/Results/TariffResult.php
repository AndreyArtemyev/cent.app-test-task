<?php


namespace App\Services\Tariff\Results;

use Money\Money;

class TariffResult
{
    private Money $amount;
    private float $feePercent;
    private Money $feeFix;
    private Money $feeMin;

    /**
     * @param Money $amount
     * @param float $feePercent
     * @param Money $feeFix
     * @param Money $feeMin
     */
    public function __construct(Money $amount, float $feePercent, Money $feeFix, Money $feeMin)
    {
        $this->amount     = $amount;
        $this->feePercent = $feePercent;
        $this->feeFix     = $feeFix;
        $this->feeMin     = $feeMin;
    }

    static function createTariff(Money $amount, string $feePercent, string $feeFix, string $feeMin ):self
    {
        return new self (
            $amount,
            floatval($feePercent),
            Money::{$amount->getCurrency()->getCode()}($feeFix),
            Money::{$amount->getCurrency()->getCode()}($feeMin)
        );
    }

    /**
     * Метод возвращает значение Amount.
     *
     * @return Money
     */
    public function getAmount(): Money
    {
        return $this->amount;
    }

    /**
     * Метод возвращает значение FeePercent.
     *
     * @return float
     */
    public function getFeePercent(): float
    {
        return $this->feePercent;
    }

    /**
     * Метод возвращает значение FeeFix.
     *
     * @return Money
     */
    public function getFeeFix(): Money
    {
        return $this->feeFix;
    }

    /**
     * Метод возвращает значение FeeMin.
     *
     * @return Money
     */
    public function getFeeMin(): Money
    {
        return $this->feeMin;
    }

}