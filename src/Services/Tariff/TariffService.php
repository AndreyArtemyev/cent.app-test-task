<?php

namespace App\Services\Tariff;

use App\Exception\PaymentCurrencyNotFound;
use App\Exception\TariffNotFound;
use App\Services\Tariff\Results\TariffResult;
use Money\Money;

class TariffService
{
    public function handle(Money $amount, array $commissionTable): TariffResult
    {
        $currency = $amount->getCurrency()->getCode();
        if (isset($commissionTable[$currency])) {
            foreach ($commissionTable[$currency] as $sum => $tariff) {
                if ((int)$amount->getAmount() <= $sum || array_key_last($commissionTable[$currency]) === $sum){
                    return TariffResult::createTariff(
                        $amount,
                        $tariff['free_percent'],
                        $tariff['free_fix'],
                        $tariff['free_min'],
                    );
                }
            }
        } else {
            throw new PaymentCurrencyNotFound();
        }
        throw new TariffNotFound();
    }
}