<?php

namespace App\Services\Notification;

use App\Banks\Responses\Payment as PaymentResult;
use App\Entities\Payment;
use App\PaymentMethods\Card;

class SendNotificationService
{
    const SUCCESS_MESSAGE = 'Thank you! Payment completed';
    const WRONG_MESSAGE = 'Something went wrong! Try another card';

    public static function send(PaymentResult $response, Payment $payment): string
    {
        if ($response->isCompleted()) {
            if (Card::METHOD_NAME === $payment->getPaymentMethod()->getMethodName() && 'EUR' === $payment->getAmount()->getCurrency()->getCode()) {
                return self::SUCCESS_MESSAGE . ' ' .$payment->getNetAmount()->getAmount();
            }
            return self::SUCCESS_MESSAGE;
        } elseif ($response->isFailed()) {
            return self::WRONG_MESSAGE;
        }
    }
}