<?php


namespace App\Services\Payments\Commands;

use App\PaymentMethods\PaymentMethodInterface;
use Money\Money;

class CreatePaymentCommand
{

    private Money $amount;
    private PaymentMethodInterface $paymentMethod;

    public function __construct(Money $amount, PaymentMethodInterface $paymentMethod)
    {
        $this->amount = $amount;
        $this->paymentMethod = $paymentMethod;
    }

    public function getAmount(): Money
    {
        return $this->amount;
    }

    public function getPaymentMethod(): PaymentMethodInterface
    {
        return $this->paymentMethod;
    }
}