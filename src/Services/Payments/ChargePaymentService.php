<?php

namespace App\Services\Payments;

use App\Banks\BankInterface;
use App\Entities\Payment;

class ChargePaymentService
{
    public function handle(Payment $payment, BankInterface $bank): \App\Banks\Responses\Payment
    {
        return $bank->createPayment($payment->getNetAmount(), $payment->getPaymentMethod());
    }
}