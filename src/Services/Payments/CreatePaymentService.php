<?php

namespace App\Services\Payments;

use App\Entities\Payment;
use App\Services\Commission\CommissionService;
use App\Services\Payments\Commands\CreatePaymentCommand;

class CreatePaymentService
{
    public function handle(CreatePaymentCommand $command): Payment
    {
        $commission = (new CommissionService())->commission($command->getAmount(), $command->getPaymentMethod());
        return new Payment($command->getAmount(), $commission, $command->getPaymentMethod());
    }
}