<?php

namespace App\Services\Commission;

use App\PaymentMethods\PaymentMethodInterface;
use App\Services\Commission\Results\CommissionResult;
use Money\Money;

class CommissionService extends BaseService
{
    /**
     * Расчет комиссии
     * @param Money $service
     * @param PaymentMethodInterface $formSummaryDto
     * @return CommissionResult
     */
    public function commission($amount, PaymentMethodInterface $paymentMethod): CommissionResult
    {
        return $this->getAdapterFromPaymentMethod($paymentMethod)->calculateCommission($amount);
    }
}