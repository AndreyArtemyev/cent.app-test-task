<?php


namespace App\Services\Commission\Results;

use Money\Money;

class CommissionResult
{
    // Комиссия добавляется к сумма платежа
    // Будет списано больше указанной суммы
    public const UP = 'up';

    // Комиссия входит в сумму платежа
    // Будет зачислено меньше списанного
    public const DOWN = 'down';

    private Money $amount;
    private string $commissionType;

    public function __construct(Money $amount, string $commissionType)
    {
        $this->amount = $amount;
        $this->commissionType = $commissionType;
    }

    public function getAmount(): Money
    {
        return $this->amount;
    }

    public function getCommissionType(): string
    {
        return $this->commissionType;
    }
}