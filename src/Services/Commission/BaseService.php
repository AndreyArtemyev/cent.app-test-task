<?php

namespace App\Services\Commission;

use App\PaymentMethods\Card;
use App\PaymentMethods\PaymentMethodInterface;
use App\PaymentMethods\Qiwi;
use App\Services\Adapter\AdapterInterface;
use App\Services\Adapter\CardAdapter;
use App\Services\Adapter\QiwiAdapter;
use App\Services\Adapter\StandardAdapter;

class BaseService
{
    public function getAdapterFromPaymentMethod(PaymentMethodInterface $paymentMethod): AdapterInterface
    {
        switch ($paymentMethod->getMethodName()) {
            case Card::METHOD_NAME:
                return new CardAdapter();
            case Qiwi::METHOD_NAME:
                return new QiwiAdapter();
            default:
                return new StandardAdapter();
        }
    }
}