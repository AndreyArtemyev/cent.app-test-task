<?php

namespace App\Services\Adapter;

use App\Services\Commission\Results\CommissionResult;
use App\Services\Tariff\TariffService;
use Money\Money;

class CardAdapter extends StandardAdapter implements AdapterInterface
{
    const TABLE_COMMISSION = [
        'RUB' => [
            1000 => [
                'free_percent' => 0.04,
                'free_fix' => 1,
                'free_min' => 3,
            ],
            10000 => [
                'free_percent' => 0.03,
                'free_fix' => 1,
                'free_min' => 3,
            ],
        ],
        'EUR' => [
            10000 => [
                'free_percent' => 0.07,
                'free_fix' => 1,
                'free_min' => 4,
            ],
        ],
    ];

    /**
     * @param Money $amount сумма платежа
     * @return CommissionResult
     */
    public function calculateCommission(
        Money $amount
    ): CommissionResult {
        $tariffService = new TariffService();
        $tariffResult = $tariffService->handle($amount, self::TABLE_COMMISSION);
        return new CommissionResult(
            $this->calculate($tariffResult),
            CommissionResult::UP);
    }
}