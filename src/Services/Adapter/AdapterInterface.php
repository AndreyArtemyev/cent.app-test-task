<?php


namespace App\Services\Adapter;

use App\Services\Commission\Results\CommissionResult;
use Money\Money;

interface AdapterInterface
{
    /**
     * @param Money $amount сумма платежа
     * @return CommissionResult
     */
    public function calculateCommission(Money $amount): CommissionResult;
}