<?php

namespace App\Services\Adapter;

use App\Services\Commission\Results\CommissionResult;
use App\Services\Tariff\TariffService;
use Money\Money;

class QiwiAdapter extends StandardAdapter implements AdapterInterface
{
    const TABLE_COMMISSION = [
        'RUB' => [
            75000 => [
                'free_percent' => 0.05,
                'free_fix' => 0,
                'free_min' => 3,
            ],
        ],
    ];

    /**
     * @param Money $amount сумма платежа
     * @return CommissionResult
     */
    public function calculateCommission(
        Money $amount
    ): CommissionResult {
        $tariffService = new TariffService();
        $tariffResult = $tariffService->handle($amount, self::TABLE_COMMISSION);
        return new CommissionResult(
            $this->calculate($tariffResult),
            CommissionResult::UP);
    }
}