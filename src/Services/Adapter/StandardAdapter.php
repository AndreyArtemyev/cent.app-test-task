<?php

namespace App\Services\Adapter;

use App\Services\Commission\Results\CommissionResult;
use App\Services\Tariff\Results\TariffResult;
use Money\Money;

class StandardAdapter implements AdapterInterface
{
    /**
     * @param Money $amount сумма платежа
     * @return Money
     */
    public function calculateCommission(
        Money $amount
    ): CommissionResult {
        return new CommissionResult(Money::RUB(100), CommissionResult::DOWN);
    }

    /**
     * @param TariffResult $tariffResult
     * @return Money
     */
    public function calculate(
        TariffResult $tariffResult
    ): Money {
        $commissionAmount = $tariffResult->getAmount()->multiply($tariffResult->getFeePercent())->add($tariffResult->getFeeFix());
        if ($commissionAmount->greaterThanOrEqual($tariffResult->getFeeMin())) {
            return $commissionAmount;
        } else {
            return $tariffResult->getFeeMin();
        }
    }
}