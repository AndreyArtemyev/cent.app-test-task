<?php


namespace App\Entities;


use App\PaymentMethods\PaymentMethodInterface;
use App\Services\Commission\Results\CommissionResult;
use DateTime;
use Money\Money;

class Payment
{
    private Money $amount;
    private CommissionResult $commissionResult;
    private PaymentMethodInterface $paymentMethod;
    private DateTime $createdAt;

    public function __construct(
        Money $amount,
        CommissionResult $commissionResult,
        PaymentMethodInterface $paymentMethod
    ) {
        $this->amount = $amount;
        $this->commissionResult = $commissionResult;
        $this->paymentMethod = $paymentMethod;
        $this->createdAt = new DateTime();
    }

    public function getAmount(): Money
    {
        return $this->amount;
    }

    public function getCommission(): CommissionResult
    {
        return $this->commissionResult;
    }

    public function getPaymentMethod(): PaymentMethodInterface
    {
        return $this->paymentMethod;
    }

    public function getCreatedAt(): DateTime
    {
        return $this->createdAt;
    }

    public function getNetAmount(): Money
    {
        if ($this->commissionResult->getCommissionType() === CommissionResult::DOWN){
            return $this->amount->subtract($this->commissionResult->getAmount());
        }
        else {
            return $this->amount->add($this->commissionResult->getAmount());
        }
    }

}