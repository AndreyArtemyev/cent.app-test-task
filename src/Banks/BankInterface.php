<?php


namespace App\Banks;

use App\Banks\Responses\Payment;
use App\PaymentMethods\PaymentMethodInterface;
use Money\Money;

interface BankInterface
{
    public function createPayment(Money $amount, PaymentMethodInterface $paymentMethod): Payment;
}