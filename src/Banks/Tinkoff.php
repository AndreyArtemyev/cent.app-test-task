<?php


namespace App\Banks;

use App\Banks\Responses\Payment;
use App\PaymentMethods\Card;
use App\PaymentMethods\PaymentMethodInterface;
use Money\Money;

class Tinkoff implements BankInterface
{
    public function createPayment(Money $amount, PaymentMethodInterface $paymentMethod): Payment
    {
        $compareValue = Money::{$amount->getCurrency()->getCode()}(15000);
        if (Card::METHOD_NAME === $paymentMethod->getMethodName() && $amount->greaterThanOrEqual($compareValue)){
            return new Payment(Payment::STATUS_COMPLETED);
        }
        return new Payment(Payment::STATUS_FAILED);
    }
}