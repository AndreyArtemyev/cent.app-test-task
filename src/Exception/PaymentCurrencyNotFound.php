<?php

namespace App\Exception;


class PaymentCurrencyNotFound extends \DomainException
{
    protected $code = DomainExceptionCode::PAYMENT_CURRENCY_NOT_FOUND;
    protected $message = 'Перевод по данной валюте не возможен';
}
