<?php

namespace App\Exception;

class DomainExceptionCode
{
    public const PAYMENT_CURRENCY_NOT_FOUND = 1000;
    public const TARIFF_NOT_FOUND = 1001;

}
