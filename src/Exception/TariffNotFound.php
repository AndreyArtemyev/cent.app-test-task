<?php

namespace App\Exception;


class TariffNotFound extends \DomainException
{
    protected $code = DomainExceptionCode::TARIFF_NOT_FOUND;
    protected $message = 'Tариф для данного платежа не прудусмотрен логикой';
}
