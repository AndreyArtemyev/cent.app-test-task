<?php

use App\Banks\Sberbank;
use App\Banks\Tinkoff;
use App\PaymentMethods\Card;
use App\PaymentMethods\Qiwi;
use App\Services\Notification\SendNotificationService;
use App\Services\Payments\ChargePaymentService;
use App\Services\Payments\Commands\CreatePaymentCommand;
use App\Services\Payments\CreatePaymentService;
use Money\Money;

require_once './vendor/autoload.php';

$createPaymentService = new CreatePaymentService();
$card = new Card('4242424242424242', new \DateTime('2021-10-15'), 123);
$payment = $createPaymentService->handle(new CreatePaymentCommand(Money::RUB(10000), $card));
$chargePaymentService = new ChargePaymentService();
$response = $chargePaymentService->handle($payment, new Sberbank());
echo SendNotificationService::send($response, $payment);
echo PHP_EOL;

//1
$qiwi = new Qiwi('89138777599');
$paymentQiwi = $createPaymentService->handle(new CreatePaymentCommand(Money::RUB(15000), $qiwi));
$chargePaymentService = new ChargePaymentService();
$response = $chargePaymentService->handle($paymentQiwi, new Sberbank());
echo SendNotificationService::send($response, $paymentQiwi);
echo PHP_EOL;

//2
$paymentCard = $createPaymentService->handle(new CreatePaymentCommand(Money::EUR(9000), $card));
$chargePaymentService = new ChargePaymentService();
$response = $chargePaymentService->handle($paymentCard, new Sberbank());
echo SendNotificationService::send($response, $paymentCard);
echo PHP_EOL;

//Pay tinkoff
$paymentCard = $createPaymentService->handle(new CreatePaymentCommand(Money::RUB(15000), $card));
$chargePaymentService = new ChargePaymentService();
$response = $chargePaymentService->handle($paymentCard, new Tinkoff());
echo SendNotificationService::send($response, $paymentCard);
echo PHP_EOL;